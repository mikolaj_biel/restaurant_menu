package pl.mikolajbiel.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Menu {
    private final List<Dish> dishes;
    private final List<FoodSetMenuResponse> foodSets;
}
