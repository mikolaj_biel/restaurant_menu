package pl.mikolajbiel.model;

public enum DishCategory {
    MAIN,
    ADDITION,
    DRINK
}
