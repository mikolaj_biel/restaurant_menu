package pl.mikolajbiel.model;

import lombok.Getter;
import lombok.Setter;
import pl.mikolajbiel.requests.FoodSetRequest;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
public class FoodSet {

    private static AtomicInteger ID_GENERATOR = new AtomicInteger();

    private Integer id;
    private String name;
    private String description;
    private Double price;
    private List<Integer> dishIdList;

    public FoodSet(String name, String description, Double price, List<Integer> dishIdList) {
        this.id = ID_GENERATOR.getAndIncrement();
        this.name = name;
        this.description = description;
        this.price = price;
        this.dishIdList = dishIdList;
    }

    public FoodSet(FoodSetRequest foodSetRequest) {
        this.id = ID_GENERATOR.getAndIncrement();
        this.name = foodSetRequest.getName();
        this.description = foodSetRequest.getDescription();
        this.price = foodSetRequest.getPrice();
        this.dishIdList = foodSetRequest.getDishIdList();
    }

    public void update(FoodSetRequest foodSetRequest) {
        this.name = foodSetRequest.getName();
        this.description = foodSetRequest.getDescription();
        this.price = foodSetRequest.getPrice();
        this.dishIdList = foodSetRequest.getDishIdList();
    }
}
