package pl.mikolajbiel.model;

import lombok.Getter;
import lombok.Setter;
import pl.mikolajbiel.requests.DishRequest;

import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
public class Dish {

    private static AtomicInteger ID_GENERATOR = new AtomicInteger();

    private Integer id;
    private String name;
    private String description;
    private Double price;
    private DishCategory category;

    public Dish(String name, String description, Double price, DishCategory category) {
        this.id = ID_GENERATOR.getAndIncrement();
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
    }
    public Dish(DishRequest dishRequest) {
        this.id = ID_GENERATOR.getAndIncrement();
        this.name = dishRequest.getName();
        this.description = dishRequest.getDescription();
        this.price = dishRequest.getPrice();
        this.category = dishRequest.getCategory();
    }

    public void update(DishRequest dishRequest) {
        this.name = dishRequest.getName();
        this.description = dishRequest.getDescription();
        this.price = dishRequest.getPrice();
        this.category = dishRequest.getCategory();
    }
}
