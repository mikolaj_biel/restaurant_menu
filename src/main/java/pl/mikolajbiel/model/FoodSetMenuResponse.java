package pl.mikolajbiel.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class FoodSetMenuResponse {
    private Integer id;
    private String name;
    private String description;
    private Double price;
    private List<String> mainDishes;
    private List<String> additions;
    private List<String> drinks;
}
