package pl.mikolajbiel.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mikolajbiel.model.Dish;
import pl.mikolajbiel.model.DishCategory;
import pl.mikolajbiel.model.FoodSet;
import pl.mikolajbiel.model.FoodSetMenuResponse;
import pl.mikolajbiel.requests.FoodSetRequest;
import pl.mikolajbiel.service.dao.FoodSetDao;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FoodSetService {
    private final FoodSetDao foodSetDao;
    private final DishService dishService;

    public FoodSet getFoodSet(Integer foodSetId) {
        return foodSetDao.getFoodSetById(foodSetId);
    }

    public List<FoodSet> getAllFoodSets() {
        return foodSetDao.getFoodSets();
    }

    public Integer createNewFoodSet(FoodSetRequest foodSetRequest) {
        if(validateDishes(foodSetRequest.getDishIdList())){
            FoodSet newFoodSet = new FoodSet(foodSetRequest);
            return foodSetDao.createNewFoodSet(newFoodSet);
        }
        return -1;
    }

    public boolean updateFoodSet(Integer foodSetId, FoodSetRequest foodSetRequest) {
        if(getFoodSet(foodSetId) != null && validateDishes(foodSetRequest.getDishIdList())){
            foodSetDao.updateFoodSet(foodSetId, foodSetRequest);
            return true;
        }

        return false;
    }

    private boolean validateDishes(List<Integer> dishIdList){
        return dishService.validateDishes(dishIdList);
    }

    public FoodSetMenuResponse transformToResponse(FoodSet foodSet) {

        List<Dish> dishList = dishService.getDishes(foodSet.getDishIdList());

        List<String> mainDishesNames = dishList
                .stream()
                .filter(dish -> dish.getCategory().equals(DishCategory.MAIN))
                .map(Dish::getName)
                .collect(Collectors.toList());

        List<String> additionsNames = dishList
                .stream()
                .filter(dish -> dish.getCategory().equals(DishCategory.ADDITION))
                .map(Dish::getName)
                .collect(Collectors.toList());

        List<String> drinksNames = dishList
                .stream()
                .filter(dish -> dish.getCategory().equals(DishCategory.DRINK))
                .map(Dish::getName)
                .collect(Collectors.toList());

        return new FoodSetMenuResponse(
                foodSet.getId(),
                foodSet.getName(),
                foodSet.getDescription(),
                foodSet.getPrice(),
                mainDishesNames,
                additionsNames,
                drinksNames
        );
    }

    public boolean removeFoodSet(Integer foodSetId) {
        return foodSetDao.removeFoodSetById(foodSetId);
    }
}
