package pl.mikolajbiel.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mikolajbiel.model.Dish;
import pl.mikolajbiel.model.FoodSet;
import pl.mikolajbiel.model.FoodSetMenuResponse;
import pl.mikolajbiel.model.Menu;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MenuService {

    private final FoodSetService foodSetService;
    private final DishService dishService;


    public Menu getFullMenu() {
        List<Dish> allDishes = dishService.getAllDishes();
        List<FoodSet> allFoodSets = foodSetService.getAllFoodSets();
        List<FoodSetMenuResponse> foodSetsResponse = allFoodSets
                .stream()
                .map(foodSetService::transformToResponse)
                .collect(Collectors.toList());

        return new Menu(allDishes, foodSetsResponse);
    }
}
