package pl.mikolajbiel.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mikolajbiel.model.Dish;
import pl.mikolajbiel.model.DishCategory;
import pl.mikolajbiel.requests.DishRequest;
import pl.mikolajbiel.service.dao.DishDao;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DishService {
    private final DishDao dishDao;

    public Dish getDish(Integer dishId) {
        return dishDao.getDish(dishId);
    }

    public List<Dish> getAllDishes() {
        return dishDao.getAllDishes();
    }

    public Integer createNewDish(DishRequest dishRequest) {
        Dish newDish = new Dish(dishRequest);
        return dishDao.storeNewDish(newDish);
    }

    public boolean updateDish(Integer dishId, DishRequest dishRequest) {
        if(getDish(dishId) == null){
            return false;
        }
        dishDao.updateDish(dishId, dishRequest);
        return true;
    }

    public List<Dish> getDishByCategory(DishCategory dishCategory) {
        return dishDao.getAllDishes()
                .stream()
                .filter(dish -> dish.getCategory().equals(dishCategory))
                .collect(Collectors.toList());
    }

    public boolean validateDishes(List<Integer> dishIdList) {
        for (Integer dishId : dishIdList) {
            if(getDish(dishId) == null){
                return false;
            }
        }
        return true;
    }

    public List<Dish> getDishes(List<Integer> dishIdList) {
        return getAllDishes()
                .stream()
                .filter(dish -> dishIdList.contains(dish.getId()))
                .collect(Collectors.toList());
    }

    public boolean removeDish(Integer dishId) {
        return dishDao.removeDishById(dishId);
    }
}
