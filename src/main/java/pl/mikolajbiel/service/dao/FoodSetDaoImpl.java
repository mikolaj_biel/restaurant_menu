package pl.mikolajbiel.service.dao;

import org.springframework.stereotype.Repository;
import pl.mikolajbiel.model.FoodSet;
import pl.mikolajbiel.requests.FoodSetRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FoodSetDaoImpl implements FoodSetDao {
    //Temporary storing in List - can be moved to database
    Map<Integer, FoodSet> listOfFoodSets = new HashMap<>();

    public FoodSetDaoImpl() {
        //Add basic food set with predefined dishes - TEST purpose only
        List<Integer> happyMealDishIdList = new ArrayList<>();
        happyMealDishIdList.add(0);
        happyMealDishIdList.add(1);
        happyMealDishIdList.add(2);
        happyMealDishIdList.add(3);
        FoodSet happyMeal = new FoodSet("Happy Meal", "Happy Meal ideal for kids", 25.55, happyMealDishIdList);
        listOfFoodSets.put(happyMeal.getId(), happyMeal);
    }

    @Override
    public FoodSet getFoodSetById(Integer foodSetId) {
        return listOfFoodSets.get(foodSetId);
    }

    @Override
    public List<FoodSet> getFoodSets() {
        return new ArrayList<>(listOfFoodSets.values());
    }

    @Override
    public Integer createNewFoodSet(FoodSet newFoodSet) {
        listOfFoodSets.put(newFoodSet.getId(), newFoodSet);
        return newFoodSet.getId();
    }

    @Override
    public void updateFoodSet(Integer foodSetId, FoodSetRequest foodSetRequest) {
        FoodSet foodSet = listOfFoodSets.get(foodSetId);
        foodSet.update(foodSetRequest);
    }

    @Override
    public boolean removeFoodSetById(Integer foodSetId) {
        return listOfFoodSets.remove(foodSetId) != null;
    }
}
