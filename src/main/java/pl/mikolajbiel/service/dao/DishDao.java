package pl.mikolajbiel.service.dao;

import pl.mikolajbiel.model.Dish;
import pl.mikolajbiel.requests.DishRequest;

import java.util.List;

public interface DishDao {
    Dish getDish(Integer dishId);

    List<Dish> getAllDishes();

    Integer storeNewDish(Dish newDish);

    void updateDish(Integer dishId, DishRequest dishRequest);

    boolean removeDishById(Integer dishId);
}
