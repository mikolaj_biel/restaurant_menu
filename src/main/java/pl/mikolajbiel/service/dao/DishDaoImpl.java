package pl.mikolajbiel.service.dao;

import org.springframework.stereotype.Repository;
import pl.mikolajbiel.model.Dish;
import pl.mikolajbiel.model.DishCategory;
import pl.mikolajbiel.requests.DishRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DishDaoImpl implements DishDao{
    //Temporary storing in List - can be moved to database
    Map<Integer, Dish> listOfDishes = new HashMap<>();

    public DishDaoImpl() {
        //Add basic products - TEST purpose only
        Dish cola = new Dish("Cola", "0,5l of Cola", 5.23, DishCategory.DRINK);
        listOfDishes.put(cola.getId(), cola);
        Dish sprite = new Dish("Sprite", "0,5l of Sprite", 2.23, DishCategory.DRINK);
        listOfDishes.put(sprite.getId(), sprite);
        Dish fries = new Dish("Fries", "Fries", 12.23, DishCategory.ADDITION);
        listOfDishes.put(fries.getId(), fries);
        Dish cheeseburger = new Dish("Cheeseburger", "Hamburger with cheese", 10.23, DishCategory.MAIN);
        listOfDishes.put(cheeseburger.getId(), cheeseburger);
    }

    @Override
    public Dish getDish(Integer dishId) {
        return listOfDishes.get(dishId);
    }

    @Override
    public List<Dish> getAllDishes() {
        return new ArrayList<>(listOfDishes.values());
    }

    @Override
    public Integer storeNewDish(Dish newDish) {
        listOfDishes.put(newDish.getId(), newDish);
        return newDish.getId();
    }

    @Override
    public void updateDish(Integer dishId, DishRequest dishRequest) {
        Dish dish = listOfDishes.get(dishId);
        dish.update(dishRequest);
    }

    @Override
    public boolean removeDishById(Integer dishId) {
        return listOfDishes.remove(dishId) != null;
    }
}
