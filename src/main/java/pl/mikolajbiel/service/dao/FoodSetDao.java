package pl.mikolajbiel.service.dao;

import pl.mikolajbiel.model.FoodSet;
import pl.mikolajbiel.requests.FoodSetRequest;

import java.util.List;

public interface FoodSetDao {
    FoodSet getFoodSetById(Integer foodSetId);

    List<FoodSet> getFoodSets();

    Integer createNewFoodSet(FoodSet newFoodSet);

    void updateFoodSet(Integer foodSetId, FoodSetRequest foodSetRequest);

    boolean removeFoodSetById(Integer foodSetId);
}
