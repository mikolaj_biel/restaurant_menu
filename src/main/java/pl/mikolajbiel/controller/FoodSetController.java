package pl.mikolajbiel.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mikolajbiel.requests.FoodSetRequest;
import pl.mikolajbiel.service.FoodSetService;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("api/v1/foodset")
public class FoodSetController {

    private final FoodSetService foodSetService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getFoodSets(@RequestParam(required = false) Integer foodSetId) {
        if(foodSetId != null){
            return ResponseEntity.ok(foodSetService.getFoodSet(foodSetId));
        }
        return ResponseEntity.ok(foodSetService.getAllFoodSets());
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<?> removeFoodSet(@RequestParam Integer foodSetId) {
        if (foodSetService.removeFoodSet(foodSetId)){
            return ResponseEntity.ok("FoodSet with ID: " + foodSetId + " was removed correctly");
        }

        return ResponseEntity.badRequest().body("FoodSet with ID: " + foodSetId + " was not removed.");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createFoodSet(@RequestBody FoodSetRequest foodSetRequest) {
        Integer foodSetId = foodSetService.createNewFoodSet(foodSetRequest);
        if(foodSetId >= 0){
            return ResponseEntity.ok("New FoodSet was created with ID: " + foodSetId);
        } else {
            return ResponseEntity.badRequest().body("New FoodSet could not be created");
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateFoodSet(@RequestParam Integer foodSetId, @RequestBody FoodSetRequest foodSetRequest) {
        if(foodSetService.updateFoodSet(foodSetId, foodSetRequest)) {
            return ResponseEntity.ok("FoodSet with ID: " + foodSetId + " was updated correctly");
        } else {
            return ResponseEntity.badRequest().body("Cannot update FoodSet with ID: " + foodSetId);
        }
    }
}
