package pl.mikolajbiel.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mikolajbiel.model.Dish;
import pl.mikolajbiel.model.DishCategory;
import pl.mikolajbiel.requests.DishRequest;
import pl.mikolajbiel.service.DishService;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("api/v1/dish")
public class DishController {

    private final DishService dishService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getDish(@RequestParam(required = false) Integer dishId) {
        if(dishId != null){
            Dish dish = dishService.getDish(dishId);
            return ResponseEntity.ok(dish);
        }
        List<Dish> allDishes = dishService.getAllDishes();
        return ResponseEntity.ok(allDishes);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<?> removeDish(@RequestParam Integer dishId) {
        if (dishService.removeDish(dishId)){
            return ResponseEntity.ok("Dish with ID: " + dishId + " was removed correctly");
        }

        return ResponseEntity.badRequest().body("Dish with ID: " + dishId + " was not removed.");
    }

    @RequestMapping(
            value = "/category",
            method = RequestMethod.GET)
    public ResponseEntity<?> getDishByCategory(@RequestParam String dishCategory) {
        DishCategory finalDishCategory;
        try {
            finalDishCategory = Enum.valueOf(DishCategory.class, dishCategory);
        } catch (Exception e){
            return ResponseEntity.badRequest().body("Provided dish category does not match any categories from: " + Arrays.toString(DishCategory.values()));
        }

        List<Dish> dishes = dishService.getDishByCategory(finalDishCategory);
        return ResponseEntity.ok(dishes);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createSingleDish(@RequestBody DishRequest dishRequest) {
        Integer dishId = dishService.createNewDish(dishRequest);
        if(dishId >= 0){
            return ResponseEntity.ok("New dish was created with ID: " + dishId);
        } else {
            return ResponseEntity.badRequest().body("New Dish could not be created");
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateSelectedDish(@RequestParam Integer dishId, @RequestBody DishRequest dishRequest) {
        if(dishService.updateDish(dishId, dishRequest)) {
            return ResponseEntity.ok("Dish with ID: " + dishId + " was updated correctly");
        } else {
            return ResponseEntity.badRequest().body("Cannot update Dish with ID: " + dishId);
        }
    }
}
