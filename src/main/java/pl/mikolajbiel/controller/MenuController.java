package pl.mikolajbiel.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mikolajbiel.service.MenuService;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("api/v1")
public class MenuController {

    private final MenuService menuService;

    @RequestMapping(
            value = "menu",
            method = RequestMethod.GET)
    public ResponseEntity<?> getFullMenu() {

        return ResponseEntity.ok(menuService.getFullMenu());
    }
}
