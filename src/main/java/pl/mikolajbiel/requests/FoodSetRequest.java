package pl.mikolajbiel.requests;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FoodSetRequest {
    private String name;
    private String description;
    private Double price;
    private List<Integer> dishIdList;
}
