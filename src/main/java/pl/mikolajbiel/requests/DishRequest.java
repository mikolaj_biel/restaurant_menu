package pl.mikolajbiel.requests;

import lombok.Getter;
import lombok.Setter;
import pl.mikolajbiel.model.DishCategory;

@Getter
@Setter
public class DishRequest {
    private String name;
    private String description;
    private Double price;
    private DishCategory category;
}
