# README #

### What is this repository for? ###

* Providing prototype for restaurant menu application

### Functionalities ###
* Providing restaurant with ability to create their own menu

### Dishes categories available: ###
* MAIN
* ADDITION
* DRINK

### Workflow: ###
* Adding multiple dishes in multiple categories (POST /api/v1/dish)
```json
{
    "name": "Apple",
    "description": "One big apple",
    "price": 25.23,
    "category": "ADDITION"
}
```

* Creating FoodSet from already created dishes (POST /api/v1/foodset)
```json
{
    "name": "Kebab XL",
    "description": "Kebab for everyone",
    "price": 15.85,
    "dishIdList": [
        4,5,6
    ]
}
```
* Listing whole restaurant menu (GET /api/v1/menu)

### Endpoints: ###
* /api/v1/dish GET
* /api/v1/dish POST/PUT {dishId parameter required}
* /api/v1/dish/category GET {dishCategory parameter required}
* /api/v1/foodset GET
* /api/v1/foodset POST/PUT {foodSetId parameter required}
* /api/v1/menu GET

